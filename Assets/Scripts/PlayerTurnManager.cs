﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerTurn
{
    A = 0,
    B = 1,
}

public class Player
{
    public int score = 0;
    public int playerShotCounts = 0;
    public int previousScore = 0;
    public Image[] playerScoreImage;
    public Player opponent;
    public string name;

    public Player(string name)
    {
        this.name = name;
    }
}

public class PlayerTurnManager : MonoBehaviour
{
    private Player m_playerA = new Player("A"), m_playerB = new Player("B"), m_currentPlayer;

    public Player playerA
    {
        set { m_playerA = value; }
        get { return m_playerA; }
    }

    public Player playerB
    {
        set { m_playerB = value; }
        get { return m_playerB; }
    }

    public Player currentPlayer
    {
        set { m_currentPlayer = value; }
        get { return m_currentPlayer; }
    }

    private static PlayerTurnManager instance;
    public static PlayerTurnManager Instance { get; private set; }

    [SerializeField] private Text playerTurnText;

    private PlayerTurn playerTurn = PlayerTurn.A;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        PlayerSetUp();
        RandomSelectPlayerTurn();
    }

    private void PlayerSetUp()
    {
        m_playerA.opponent = playerB;
        m_playerB.opponent = playerA;
    }

    private void ChangeToTurnA()
    {
        playerTurn = PlayerTurn.A;
        m_currentPlayer = playerA;
        UpdatePlayerTurnText();
    }

    private void ChangeToTurnB()
    {
        playerTurn = PlayerTurn.B;
        m_currentPlayer = playerB;
        UpdatePlayerTurnText();
    }

    public void SwapPlayerTurn()
    {
        if (playerTurn == PlayerTurn.A)
        {
            ChangeToTurnB();
        }
        else if (playerTurn == PlayerTurn.B)
        {
            ChangeToTurnA();
        }
    }

    private void RandomSelectPlayerTurn()
    {
        int randomValue = UnityEngine.Random.Range(0, 2);
        switch (randomValue)
        {
            case 0:
                ChangeToTurnA();
                break;
            case 1:
                ChangeToTurnB();
                break;
            default:
                break;
        }
    }

    private void UpdatePlayerTurnText()
    {
        if (playerTurnText)
        {
            switch (playerTurn)
            {
                case PlayerTurn.A:
                    playerTurnText.text = m_playerA.name;
                    break;
                case PlayerTurn.B:
                    playerTurnText.text = m_playerB.name;
                    break;
                default:
                    break;
            }
        }
    }
}
