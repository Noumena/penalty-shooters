﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallController : MonoBehaviour
{
    private Rigidbody rigidbody;

    [SerializeField] private Vector3[] randomDirVecArray; 
    private Vector3 footBalPos; 

    private bool isBallReturned = true;
    private bool goal = false; 
    
    [SerializeField] private float time = 5.0f;  
    private int numberOfBallShots = 0;
  
    private ScoreManager scoreManager;
    private PlayerTurnManager playerTurnManager;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        footBalPos = transform.position;        
    }

    private void Start()
    {        
        scoreManager = ScoreManager.Instance;
        playerTurnManager = PlayerTurnManager.Instance;                
    }
  
    private void OnMouseDown()
    {
        if (scoreManager && playerTurnManager && isBallReturned && !scoreManager.isGameOver)
        {
            KickBallInRandomDir();
        }
    }

    private void KickBallInRandomDir()
    {
        int randomDirIndex = UnityEngine.Random.Range(0, randomDirVecArray.Length);
        rigidbody.AddForce(randomDirVecArray[randomDirIndex]);
        isBallReturned = false;        
        StartCoroutine(ResetBall(time));
    }

    private IEnumerator ResetBall(float time)
    {
        yield return new WaitForSeconds(time);
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        transform.position = footBalPos;

        if (goal)
        {
            playerTurnManager.currentPlayer.score++;
            goal = false;
        }

        numberOfBallShots++;

        if (numberOfBallShots != 0 && numberOfBallShots % 2 == 0)
        {
            scoreManager.gameRound++;
        }
        
        playerTurnManager.currentPlayer.playerShotCounts++;
        scoreManager.UpdateScorePanel(playerTurnManager.currentPlayer);
        scoreManager.HandleScore(playerTurnManager.currentPlayer);
        playerTurnManager.SwapPlayerTurn();
        isBallReturned = true;       
    }

    private void OnTriggerEnter(Collider other)
    {
        goal = true;
    }    
}
