﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private Image[] playerAScoreImage, playerBScoreImage;

    [SerializeField] private Text gameModeText, playerTurnText, winText, scoreText;

    [SerializeField] private CanvasRenderer winPanel;

    private static ScoreManager instance;
    public static ScoreManager Instance { get; private set; }

    private int m_gameRound = 0;
    public int gameRound
    {
        set { m_gameRound = value; }
        get { return m_gameRound; }
    }

    private int RoundsToWin = 5;
    private readonly int RoundsPerMode = 5;

    private bool m_isGameOver = false, isTied = false;
    public bool isGameOver
    {
        set { m_isGameOver = value; }
        get { return m_isGameOver; }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        SetWinnerPanel();
    }

    private void Start()
    {
        SetPlayerScorePanel();
    }

    public void SetPlayerScorePanel()
    {
        PlayerTurnManager.Instance.playerA.playerScoreImage = playerAScoreImage;
        PlayerTurnManager.Instance.playerB.playerScoreImage = playerBScoreImage;
    }

    private void ResetScorePanel()
    {
        if (playerAScoreImage != null && playerBScoreImage != null)
        {
            foreach (var image in playerAScoreImage)
            {
                if (image)
                {
                    image.color = Color.white;
                }
            }

            foreach (var image in playerBScoreImage)
            {
                if (image)
                {
                    image.color = Color.white;
                }
            }
        }
    }

    public void UpdateScorePanel(Player player)
    {
        if (player != null)
        {
            if (player.previousScore == player.score)
            {
                if (player.playerScoreImage[player.playerShotCounts - 1])
                {
                    player.playerScoreImage[player.playerShotCounts - 1].color = Color.red;
                }
            }

            if (player.previousScore < player.score)
            {
                if (player.playerScoreImage[player.playerShotCounts - 1])
                {
                    player.playerScoreImage[player.playerShotCounts - 1].color = Color.green;
                }
            }

            player.previousScore = player.score;
        }        
    }

    public void HandleScore(Player player)
    {
        if (player != null)
        {
            int chanceLeftsForPlayer = RoundsToWin - player.playerShotCounts;
            int chanceLeftsForCounterPlayer = RoundsToWin - player.opponent.playerShotCounts;

            int totalChancePlayerToScores = player.score + chanceLeftsForPlayer;
            int totalChanceCounterPlayerToScores = player.opponent.score + chanceLeftsForCounterPlayer;

            if (player.score > totalChanceCounterPlayerToScores)
            {
                isGameOver = true;
                SetWinner(player);
            }

            if (player.opponent.score > totalChancePlayerToScores)
            {
                isGameOver = true;
                SetWinner(player.opponent);
            }

            if (gameRound == RoundsPerMode)
            {
                if (player.score == player.opponent.score)
                {
                    isGameOver = true;
                    isTied = true;
                    SetWinner(player);
                }
            }
        }                   
    }

    private void SetWinnerPanel()
    {
        if (winPanel)
        {
            if (winPanel.gameObject.activeSelf)
            {
                winPanel.gameObject.SetActive(false);
            }
            else if (!winPanel.gameObject.activeSelf)
            {
                winPanel.gameObject.SetActive(true);
            }
        }
    }

    private void SetWinner(Player player)
    {
        if (winText && scoreText )
        {
            if (!isTied)
            {
                winText.text = player.name + " is a Winner";
                scoreText.text =  player.score.ToString() + " - " + player.opponent.score.ToString();
            }
            else
            {
                winText.text = "Tied!!";
                scoreText.text = player.score.ToString() + " - " + player.opponent.score.ToString();
            }             
        }

        SetWinnerPanel();
    }
}
